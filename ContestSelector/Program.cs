﻿using System;
using System.Collections.Generic;

namespace ContestSelector
{
	enum ErrorCodes : int
	{
		NO_ERROR = 0,
		SETUP_FAILURE = 1,
		BAD_ARG_NUM = 2,
		BAD_PLEDGE_FORMAT = 3
	}
	class Program
	{
		static int Main(string[] args)
		{
			int err = 0;
			long thresh = 0;
			long winNum = 0;

			if (args.Length != 3)
			{
				Console.WriteLine("Expected arguments in the following format:\nContestSelector [file] [threshold] [num of winners]");
				return (int)ErrorCodes.BAD_ARG_NUM;
			}
			else
			{
				if(!System.IO.File.Exists(args[0]))
				{
					err++;
					Console.WriteLine("Supplied file does not exist or is unreadable");
				}
				
				try
				{
					thresh = Int64.Parse(args[1]);
				}
				catch(FormatException e)
				{
					err++;
					Console.WriteLine("Supplied threshold is not a valid number");
				}
				catch(OverflowException e)
				{
					err++;
					Console.WriteLine("Supplied threshold exceeds either the maximum or minimum possible values");
				}

				try
				{
					winNum = Int64.Parse(args[2]);
				}
				catch (FormatException e)
				{
					err++;
					Console.WriteLine("Supplied number of winners is not a valid number");
				}
				catch (OverflowException e)
				{
					err++;
					Console.WriteLine("Supplied number of winners exceeds either the maximum or minimum possible values");
				}
			}

			if (err != 0)
			{
				Console.WriteLine("At least " + err + " errors were encountered during setup. The program cannot continue and will now exit.");
				return (int)ErrorCodes.SETUP_FAILURE;
			}

			string[] lines = System.IO.File.ReadAllLines(args[0]);
			List<string[]> entries = new List<string[]>();

			foreach (string line in lines)
			{
				string[] breakdown;
				breakdown = line.Split(',');

				if(breakdown.Length == 15)
				{
					if(breakdown[5].Equals("Processed", StringComparison.InvariantCultureIgnoreCase))
					{
						long pledge = 0;
						try
						{
							string pledgeRaw = breakdown[3];
							pledgeRaw = pledgeRaw.Remove(pledgeRaw.IndexOf('.'), 1);
							//Console.Write(pledgeRaw + "\n");

							pledge = Int64.Parse(pledgeRaw);
						}
						catch(Exception e)
						{
							Console.Write("Error getting pledge amount! This probably indicates a problem with the supplied file. Exception message follows: " + e.Message + "\n");
							return (int)ErrorCodes.BAD_PLEDGE_FORMAT;
						}

						if(pledge >= (thresh * 100))
							entries.Add(breakdown);
					}
				}
			}
			Random rand = new Random();
			for (int iii = 0; iii < winNum; iii++)
			{
				int sel = rand.Next(entries.Count);
				Console.Write(entries[sel][0] + " " + entries[sel][1] + " " + entries[sel][2] + "\n");
				entries.RemoveAt(sel);
			}

			return (int)ErrorCodes.NO_ERROR;
		}
	}
}
